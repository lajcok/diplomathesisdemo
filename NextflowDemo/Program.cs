﻿using System.Linq;
using NextflowAdapter;

namespace NextflowDemo
{
    internal static class Program
    {
        private static void Main()
        {
            var jobManager = new JobManager();
            var job = jobManager.Add(new Job
            {
                Script = @"
for i in {1..10000}
do
    echo $i
done
",
            });

            var jobs = jobManager.List().ToArray();
        }
    }
}