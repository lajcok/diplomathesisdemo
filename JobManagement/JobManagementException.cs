using System;

namespace JobManagement
{
    public class JobManagementException : Exception
    {
        public JobManagementException(string message) : base(message) {}
    }
}