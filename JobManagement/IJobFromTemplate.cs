using Newtonsoft.Json.Linq;

namespace JobManagement
{
    public interface IJobFromTemplate : IJob
    {
        public JToken? Parameters { get; }
        IJobTemplate Template { get; }
    }
}