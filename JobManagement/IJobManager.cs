using System.Collections.Generic;

namespace JobManagement
{
    public interface IJobManager
    {
        IEnumerable<IJob> List();
        IJob Add(IJob job);
        bool Drop(string uid);
    }
}