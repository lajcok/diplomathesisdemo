namespace JobManagement
{
    public interface IJob
    {
        public string? Uid { get; }
        public string? Name { get; }
        public string? Script { get; }
        public string? ScriptFile { get; }
    }
}