﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace JobManagement
{
    public interface IJobTemplate
    {
        string Name { get; }
        JSchema? Parameters { get; }
        IJobFromTemplate Derive(string? name, JToken? parameters = null);
    }
}