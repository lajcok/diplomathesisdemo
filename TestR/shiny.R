if (!require('shiny')) {
  install.packages('shiny')
}
library(shiny)

if (!require('ggplot2')) {
  install.packages('ggplot2')
}
library(ggplot2)

if (!require('svglite')) {
  install.packages('svglite')
}
library(svglite)


### interactive SVG example via https://stackoverflow.com/questions/39093777/renderimage-and-svg-in-shiny-app

ui <- shinyUI(pageWithSidebar(
  headerPanel("renderSVG example"),
  sidebarPanel(
    sliderInput("obs", "Number of observations:",
                min = 0, max = 1000, value = 500),
    actionButton("savePlot", "savePlot")
  ),
  mainPanel(
  # Use imageOutput to place the image on the page
  imageOutput("plot"),
  imageOutput("plot_as_svg")
  )
))

server <- shinyServer(function(input, output, session) {

  ## create plot with renderPlot()

  output$plot <- renderPlot({
    ggplot(data.frame(x = rnorm(input$obs)), aes(x = x)) +
      ggtitle('Generated in renderPlot()') +
      geom_histogram()
    #hist(rnorm(input$obs), main = "Generated in renderPlot()")
  })

  ## create .svg file of the plot and render it with renderImage()

  output$plot_as_svg <- renderImage({

    width <- session$clientData$output_plot_width
    height <- session$clientData$output_plot_height
    mysvgwidth <- width / 96
    mysvgheight <- height / 96

    # A temp file to save the output.
    # This file will be removed later by renderImage

    outfile <- tempfile(fileext = '.svg')

    # Generate the svg
    plot <- ggplot(data.frame(x = rnorm(input$obs)), aes(x = x)) +
      ggtitle('Generated in renderPlot()') +
      geom_histogram()
    ggsave(outfile, plot)
    #svg(outfile, width = mysvgwidth, height = mysvgheight)
    #hist(rnorm(input$obs), main = "Generated in renderImage()")
    #dev.off()

    # Return a list containing the filename
    list(src = normalizePath(outfile),
         contentType = 'image/svg+xml',
         width = width,
         height = height,
         alt = "My Histogram")
  })

})

shinyApp(ui, server)
