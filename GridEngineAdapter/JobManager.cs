using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JobManagement;
using ShellToolkit;

namespace GridEngineAdapter
{
    /// <summary>
    /// Grid Engine adapter class, uses CLI interface – <c>q*</c> commands
    /// </summary>
    public class JobManager : IJobManager
    {
        /* Properties */

        private readonly IStartContext _context;

        /* Initialization */

        /// <summary>
        /// Grid Engine constructor
        /// </summary>
        /// <param name="context">Context in which to execute Grid Engine <c>q*</c> commands</param>
        public JobManager(IStartContext context)
        {
            _context = context;
        }

        /* Control */

        /// <summary>
        /// Checks status of enqueued Jobs via <c>qstat</c><br/>
        /// See <a href="http://gridscheduler.sourceforge.net/htmlman/htmlman1/qstat.html"><c>man qstat</c></a> 
        /// </summary>
        /// <returns>Returns queued Jobs</returns>
        /// <exception cref="GridEngineException">In case of non-zero command exit code</exception>
        /// <exception cref="QParsers.QParseException"></exception>
        public IEnumerable<IJob> List()
        {
            var result = _context.Start("qstat -r -xml");
            if (result.ExitCode != 0)
            {
                throw new GridEngineException($"qstat exited with non-zero code ({result.ExitCode})");
            }

            return result.StandardOutput.QStatusParse();
        }

        public IJob Add(IJob job)
        {
            var qSub = new StringBuilder("qsub");
            if (job.Name != null)
            {
                qSub.Append($" -N \"{job.Name.Escape()}\"");
            }

            var result = _context.Start(qSub.ToString(), job.Script);
            if (result.ExitCode != 0)
            {
                throw new GridEngineException($"qsub exited with non-zero code ({result.ExitCode})");
            }

            return result.StandardOutput.QSubmitParse();
        }

        public bool Drop(string uid)
        {
            var qDel = $"qdel {uid}";
            var result = _context.Start(qDel);
            var delStat = result.StandardOutput.QDeleteParse().ToArray().First();
            return delStat == QParsers.DeletionStatus.Deleted || delStat == QParsers.DeletionStatus.Registered;
        }

        [Serializable]
        public class GridEngineException : Exception
        {
            internal GridEngineException(string message) : base(message)
            {
            }
        }
    }
}