using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using JobManagement;

namespace GridEngineAdapter
{
    /// <summary>
    /// Parser tools of <c>q*</c> commands XML outputs of Grid Engine<br/>
    /// </summary>
    public static class QParsers
    {
        /// <summary>
        /// Parses <c>qstat -xml</c> output
        /// </summary>
        /// <param name="qStatOut"><c>qstat -xml</c> formatted output</param>
        /// <returns>Returns <see cref="IJob"/>s parsed from <see cref="qStatOut"/></returns>
        /// <exception cref="QParseException">Parsing failure, e.g. XML parsing, invalid structure...</exception>
        public static IEnumerable<IJob> QStatusParse(this string qStatOut)
        {
            var xml = new XmlDocument();
            try
            {
                xml.LoadXml(qStatOut);
            }
            catch (XmlException)
            {
                throw new QParseException("Given output could not be parsed as XML");
            }

            IEnumerable<XmlNode> nodes;
            try
            {
                nodes = xml.DocumentElement.SelectNodes("//job_list").Cast<XmlNode>();
            }
            catch (ArgumentNullException)
            {
                throw new QParseException("Could not search via XPath");
            }

            return nodes.Select(node =>
            {
                var uid = node.SelectSingleNode("JB_job_number")?.InnerText;
                var name = node.SelectSingleNode("JB_name")?.InnerText;
                if (uid == null || name == null)
                {
                    throw new QParseException("Some of Job attributes could not be found");
                }

                try
                {
                    return new Job {Uid = uid, Name = name};
                }
                catch (FormatException)
                {
                    throw new QParseException("Job ID has invalid format and cannot be parsed as integer");
                }
                catch (OverflowException)
                {
                    throw new QParseException("Job ID is out of range for a valid integer");
                }
            });
        }

        public static IJob QSubmitParse(this string qSubOut)
        {
            var pattern = new Regex(@"^Your job (?<id>\d+) \(""(?<name>\S+)""\) has been submitted$");
            var match = pattern.Match(qSubOut);

            var uid = match.Groups["id"]?.Value;
            var name = match.Groups["name"]?.Value;

            if (!match.Success || uid == null || name == null)
            {
                throw new QParseException("Submission output could not be parsed as such");
            }

            try
            {
                return new Job {Uid = uid, Name = name};
            }
            catch (Exception exception)
            {
                if (exception is FormatException || exception is OverflowException)
                {
                    throw new QParseException("Matched job ID could not be parsed as integer");
                }

                throw;
            }
        }

        public enum DeletionStatus
        {
            Deleted,
            Registered,
            NotExist,
        }

        public static IEnumerable<DeletionStatus> QDeleteParse(this string qDelOut)
        {
            var delRegex = new Regex(@"^\S+ has deleted job (?<id>\d+)$");
            var regRegex = new Regex(@"^\S+ has registered the job (?<id>\d+) for deletion$");
            var deniedRegex = new Regex("^denied: job \"(?<id>\\d+)\" does not exist$");

            return qDelOut.Split(Environment.NewLine)
                .Select(line =>
                {
                    if (delRegex.IsMatch(qDelOut))
                    {
                        return DeletionStatus.Deleted;
                    }

                    if (regRegex.IsMatch(qDelOut))
                    {
                        return DeletionStatus.Registered;
                    }

                    if (deniedRegex.IsMatch(qDelOut))
                    {
                        return DeletionStatus.NotExist;
                    }

                    throw new QParseException($"Deletion response could not be parsed: {line}");
                });
        }

        [Serializable]
        public class QParseException : Exception
        {
            internal QParseException(string message) : base(message)
            {
            }
        }
    }
}