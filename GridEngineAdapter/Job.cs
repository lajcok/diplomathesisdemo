using JobManagement;

namespace GridEngineAdapter
{
    public sealed class Job : IJob
    {
        /* Properties */

        public string? Uid { get; set; }
        public string? Name { get; set; }
        public string? Script { get; set; }
        public string? ScriptFile => null;

    }
}