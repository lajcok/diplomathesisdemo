using System;
using System.Collections.Generic;
using k8s;
using k8s.Models;
using Microsoft.Rest;

namespace KubernetesAdapter
{
    public sealed class JobConfigManager
    {
        /* Properties */

        private readonly Kubernetes _kubernetes;
        public readonly string ConfigName;
        private readonly string _nameSpace;

        public bool Initialized => _configMap != null;
        private V1ConfigMap _configMap;

        private V1ConfigMap ConfigMap
        {
            get
            {
                if (!Initialized)
                {
                    Initialize();
                }

                return _configMap;
            }
            set => _configMap = value;
        }

        /* Initialization */

        public JobConfigManager(Kubernetes kubernetes, string configName, string nameSpace)
        {
            _kubernetes = kubernetes;
            ConfigName = configName;
            _nameSpace = nameSpace;
        }

        public void Initialize()
        {
            try
            {
                ConfigMap = _kubernetes.ReadNamespacedConfigMap(ConfigName, _nameSpace);
            }
            catch (HttpOperationException)
            {
                try
                {
                    ConfigMap = _kubernetes.CreateNamespacedConfigMap(new V1ConfigMap
                    {
                        Metadata = new V1ObjectMeta {Name = ConfigName},
                    }, _nameSpace);
                }
                catch (HttpOperationException)
                {
                    throw new Exception("Config map could not be initialized");
                }
            }
        }

        /* Manager */

        // TODO: Cannot handle multiline scripts!
        public bool AddScript(string name, string script)
        {
            ConfigMap.Data ??= new Dictionary<string, string>();
            if (ConfigMap.Data.ContainsKey(name))
            {
                return false;
            }
            
            ConfigMap.Data.Add(name, script);

            try
            {
                ConfigMap = _kubernetes.ReplaceNamespacedConfigMap(ConfigMap, ConfigName, _nameSpace);
                return true;
            }
            catch (HttpOperationException)
            {
                return false;
            }
        }

        public bool DropScript(string name)
        {
            if (ConfigMap.Data == null || !ConfigMap.Data.Remove(name))
            {
                return false;
            }

            try
            {
                ConfigMap = _kubernetes.ReplaceNamespacedConfigMap(ConfigMap, ConfigName, _nameSpace);
                return true;
            }
            catch (HttpOperationException)
            {
                return false;
            }
        }
    }
}