﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using k8s;
using JobManagement;
using k8s.Models;
using Microsoft.Rest;

namespace KubernetesAdapter
{
    public sealed class JobManager : IJobManager
    {
        /* Properties */

        private readonly Kubernetes _kubernetes;

        private JobConfigManager _configManager;

        private JobConfigManager ConfigManager
        {
            get
            {
                if (!_configManager.Initialized)
                {
                    _configManager.Initialize();
                }

                return _configManager;
            }
            set => _configManager = value;
        }

        private readonly NamespaceInitializer _namespaceInitializer;

        private string NameSpace
        {
            get
            {
                if (!_namespaceInitializer.Initialized)
                {
                    _namespaceInitializer.Initialize();
                }

                return _namespaceInitializer.Name;
            }
        }
        
        /* Initialization */

        public JobManager(Kubernetes kubernetes, JobConfigManager configManager, string nameSpace)
        {
            _kubernetes = kubernetes;
            ConfigManager = configManager;
            _namespaceInitializer = new NamespaceInitializer(kubernetes, nameSpace);
        }

        public JobManager(Kubernetes kubernetes, string configName, string nameSpace)
            : this(kubernetes, new JobConfigManager(kubernetes, configName, nameSpace), nameSpace)
        {
        }

        /* Manager */

        public IEnumerable<IJob> List()
        {
            var jobs = _kubernetes.ListNamespacedJob(NameSpace);
            return jobs.Items.Select(job => new Job(job));
        }

        public IJob Add(IJob job)
        {
            ConfigManager.AddScript(job.Name, job.Script);
            try
            {
                var createdJob = _kubernetes.CreateNamespacedJob(new V1Job
                {
                    Metadata = new V1ObjectMeta {Name = job.Name},
                    Spec = new V1JobSpec
                    {
                        Template = new V1PodTemplateSpec
                        {
                            Spec = new V1PodSpec
                            {
                                Containers = new[]
                                {
                                    new V1Container
                                    {
                                        Name = job.Name,
                                        Image = "alpine",
                                        Command = new[] {"sh", Path.Join("/jobs", job.Name)},
                                        VolumeMounts = new[]
                                        {
                                            new V1VolumeMount
                                            {
                                                Name = "job-scripts-volume",
                                                MountPath = "/jobs",
                                            },
                                        },
                                    },
                                },
                                Volumes = new[]
                                {
                                    new V1Volume
                                    {
                                        Name = "job-scripts-volume",
                                        ConfigMap = new V1ConfigMapVolumeSource
                                        {
                                            Name = ConfigManager.ConfigName,
                                        },
                                    },
                                },
                                RestartPolicy = "Never",
                            },
                        },
                    },
                }, NameSpace);
                return new Job(createdJob)
                {
                    Script = job.Script,
                };
            }
            catch (HttpOperationException)
            {
                return default;
            }
        }

        public bool Drop(string uid)
        {
            try
            {
                _kubernetes.DeleteNamespacedJob(uid, NameSpace);
            }
            catch (HttpOperationException)
            {
                return false;
            }
            
            // TODO: Dropping scripts tends to fail. Cannot delete when in use?
            ConfigManager.DropScript(uid);
            return true;
        }
    }
}