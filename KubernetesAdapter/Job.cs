#nullable enable
using JobManagement;
using k8s.Models;

namespace KubernetesAdapter
{
    public class Job : IJob
    {
        public string? Uid { get; set; }
        public string? Name { get; set; }
        public string? Script { get; set; }
        public string? ScriptFile => null;

        public Job()
        {
        }

        public Job(V1Job job) : this()
        {
            Uid = Name = job.Name();
        }
    }
}