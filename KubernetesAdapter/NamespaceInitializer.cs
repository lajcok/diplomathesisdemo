using System;
using k8s;
using k8s.Models;
using Microsoft.Rest;

namespace KubernetesAdapter
{
    public sealed class NamespaceInitializer
    {
        /* Properties */

        private readonly Kubernetes _kubernetes;
        private readonly string _name;
        
        private V1Namespace _nameSpace;
        public bool Initialized => _nameSpace != null;

        public string Name
        {
            get
            {
                if (!Initialized)
                {
                    Initialize();
                }

                return _name;
            }
        }

        /* Initialization */
        
        public NamespaceInitializer(Kubernetes kubernetes, string name)
        {
            _kubernetes = kubernetes;
            _name = name;
        }

        public void Initialize()
        {
            try
            {
                _nameSpace = _kubernetes.ReadNamespace(_name);
            }
            catch (HttpOperationException)
            {
                try
                {
                    _nameSpace = _kubernetes.CreateNamespace(new V1Namespace
                    {
                        Metadata = new V1ObjectMeta {Name = _name},
                    });
                }
                catch (HttpOperationException)
                {
                    throw new Exception("Namespace could not be created");
                }
            }

        }
    }
}