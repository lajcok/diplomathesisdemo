using System.Collections.Generic;
using System.Linq;
using JobManagement;

namespace Backend.Providers
{
    public class TemplateProvider
    {
        /* Properties */

        private readonly IDictionary<string, IJobTemplate> _templates;
        public IEnumerable<IJobTemplate> Templates => _templates.Values;
        public IJobTemplate? this[string name]
        {
            get
            {
                _templates.TryGetValue(name, out var result);
                return result;
            }
        }

        /* Initialization */

        public TemplateProvider(IEnumerable<IJobTemplate> templates)
        {
            _templates = new Dictionary<string, IJobTemplate>(
                templates.Select(
                    t => new KeyValuePair<string, IJobTemplate>(t.Name, t)
                )
            );
        }
    }
}