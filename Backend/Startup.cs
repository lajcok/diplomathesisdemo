using System;
using System.Collections.Generic;
using Backend.Providers;
using ShellToolkit;
using JobManagement;
using k8s;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Schema;

namespace Backend
{
    public class Startup
    {
        /* Properties */

        private IConfiguration Configuration { get; }

        /* Initialization */

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /* Startup */

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Core and plugins
            services
                .AddMvcCore(options => { options.RespectBrowserAcceptHeader = true; })
                .AddNewtonsoftJson();

            // Job managers
            services
                .AddSingleton(_ =>
                    new GridEngineAdapter.JobManager(
                        new ContainerContext(Configuration["JobManagers:GridEngine:Container"])
                    )
                )
                .AddSingleton(_ =>
                    new KubernetesAdapter.JobManager(
                        new Kubernetes(KubernetesClientConfiguration.BuildConfigFromConfigFile()),
                        Configuration["JobManagers:Kubernetes:ConfigName"],
                        Configuration["JobManagers:Kubernetes:Namespace"]
                    )
                )
                .AddSingleton(_ =>
                    new NextflowAdapter.JobManager(
                        workingDirectory: Configuration["JobManagers:Nextflow:WorkingDirectory"]
                    )
                )
                .AddSingleton(sp =>
                    new Dictionary<string, Func<IJobManager>>
                    {
                        {"GridEngine", sp.GetService<GridEngineAdapter.JobManager>},
                        {"Kubernetes", sp.GetService<KubernetesAdapter.JobManager>},
                        {"Nextflow", sp.GetService<NextflowAdapter.JobManager>},
                    }[Configuration["ImplicitJobManager"]]()
                );
            
            // Templates
            services
                .AddSingleton(_ =>
                    new TemplateProvider(new[]
                    {
                        new NextflowAdapter.JobTemplate("tutorial", "tutorial")
                        {
                            Parameters = new JSchema
                            {
                                Properties =
                                {
                                    new KeyValuePair<string, JSchema>("str", new JSchema
                                    {
                                        Type = JSchemaType.String,
                                    }),
                                },
                                Required = {"str"},
                            },
                        },
                        new NextflowAdapter.JobTemplate("tail", "tail"),
                    })
                )
                .AddControllers();

            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            app.UseSwagger();
        }
    }
}