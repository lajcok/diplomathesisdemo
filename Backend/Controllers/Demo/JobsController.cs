using JobManagement;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers.Demo
{
    public class RawJobInput : IJob
    {
        public string? Uid { get; set; }
        public string? Name { get; set; }
        public string Script { get; set; }
        public string? ScriptFile => null;
    }

    [ApiController]
    [Route("demo/[controller]")]
    public class JobsController : ControllerBase
    {
        /* Properties */

        private readonly IJobManager _jobManager;

        /* Initialization */

        public JobsController(IJobManager jobManager)
        {
            _jobManager = jobManager;
        }

        /* Controller */

        [HttpGet]
        public IActionResult List() =>
            Ok(_jobManager.List());

        [HttpPost]
        public IActionResult Add([FromBody] RawJobInput job) =>
            Ok(_jobManager.Add(job));

        [HttpDelete]
        [Route("{uid}")]
        public IActionResult Drop(string uid) =>
            Ok(new {success = _jobManager.Drop(uid)});
    }
}