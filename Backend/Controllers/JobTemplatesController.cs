using System.Collections.Generic;
using Backend.Providers;
using JobManagement;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class JobTemplatesController : ControllerBase
    {
        /* Properties */

        private readonly TemplateProvider _provider;
        
        /* Initialization */

        public JobTemplatesController(TemplateProvider provider)
        {
            _provider = provider;
        }

        /* Controller */

        [HttpGet]
        public IActionResult List() => Ok(_provider.Templates);

        [HttpGet]
        [Route("{name}")]
        public IActionResult Get(string name) => Ok(_provider[name]);
    }
}