using System.Collections.Generic;
using Backend.Providers;
using JobManagement;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Backend.Controllers
{
    public class JobAssignment
    {
        public string? Name { get; set; }
        public JObject? Parameters { get; set; }
    }

    [ApiController]
    [Route("[controller]")]
    public class JobsController : ControllerBase
    {
        /* Properties */

        private readonly IJobManager _jobManager;
        private readonly TemplateProvider _templateProvider;

        /* Initialization */

        public JobsController(IJobManager jobManager, TemplateProvider templateProvider)
        {
            _jobManager = jobManager;
            _templateProvider = templateProvider;
        }

        /* Controller */

        [HttpGet]
        public IActionResult List() => Ok(_jobManager.List());

        [HttpPost]
        [Route("{name}")]
        public IActionResult Add(string name, [FromBody] JobAssignment assignment)
        {
            var template = _templateProvider[name];
            if (template == null)
                return NotFound($"No template named \"{name}\" has been found");

            var job = template.Derive(assignment.Name, assignment.Parameters);
            try
            {
                return Ok(_jobManager.Add(job));
            }
            catch (JobManagementException exception)
            {
                return BadRequest(new Dictionary<string, string> {{"error", exception.Message}});
            }
        }
    }
}