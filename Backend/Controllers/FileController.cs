using System.IO;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FileController : ControllerBase
    {
        private readonly IHostEnvironment _hostEnvironment;

        public FileController(IHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
        }

        [HttpPost]
        [Route("data")]
        public async Task<IActionResult> Upload()
        {
            // TODO use multipart/form-data
            // TODO text files only?
            var filePath = $"{_hostEnvironment.ContentRootPath}/RSpace/data";
            await using var writer = System.IO.File.Create(filePath);
            await Request.BodyReader.CopyToAsync(writer);
            return Ok($"Written to: {filePath}");
        }

        [HttpGet]
        [Route("plot.jpg")]
        public async Task<IActionResult> GetPlot()
        {
            var filePath = $"{_hostEnvironment.ContentRootPath}/RSpace/plot.jpg";
            try
            {
                Response.ContentType = MediaTypeNames.Image.Jpeg;
                await using var reader = System.IO.File.OpenRead(filePath);
                await reader.CopyToAsync(Response.Body);
                return Ok();
            }
            catch (FileNotFoundException)
            {
                return NotFound();
            }
        }
    }
}