#!/usr/bin/env nextflow

process tail {
    """
    tail -f /dev/null
    """
}
