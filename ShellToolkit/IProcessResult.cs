using System.Diagnostics;

namespace ShellToolkit
{
    /// <summary>
    /// Executed <see cref="Process"/> result abstraction
    /// </summary>
    public interface IProcessResult
    {
        /// <value>
        /// Reference to executed <see cref="Process"/>
        /// </value>
        public Process Process { get; }

        /// <value>
        /// Standard output collected from <see cref="Process"/>
        /// </value>
        public string StandardOutput { get; }
        
        /// <value>
        /// Standard error collected from <see cref="Process"/>
        /// </value>
        public string StandardError { get; }

        /// <value>
        /// <see cref="Process"/> exit code
        /// </value>
        public int ExitCode { get; }
    }
}