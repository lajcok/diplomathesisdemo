using System.Diagnostics;

namespace ShellToolkit
{
    /// <summary>
    /// Result of an executed <see cref="Process"/>
    /// </summary>
    public class ProcessResult : IProcessResult
    {
        /// <value>
        /// Reference to executed <see cref="Process"/>
        /// </value>
        public Process Process { get; }
            
        /// <value>
        /// Standard output collected from <see cref="Process"/>
        /// </value>
        public string StandardOutput { get; }
            
        /// <value>
        /// Standard error collected from <see cref="Process"/>
        /// </value>
        public string StandardError { get; }
            
        /// <value>
        /// <see cref="Process"/> exit code shorthand
        /// </value>
        public int ExitCode => Process.ExitCode;

        /// <summary>
        /// The constructor
        /// </summary>
        /// <param name="process">Reference to executed process</param>
        /// <param name="standardOutput">Standard output collected from Process</param>
        public ProcessResult(Process process, string standardOutput = "", string standardError = "")
        {
            
            Process = process;
            StandardOutput = standardOutput;
            StandardError = standardError;
        }
    }
}