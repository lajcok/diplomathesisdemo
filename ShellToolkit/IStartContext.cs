using System.Threading.Tasks;

namespace ShellToolkit
{
    public interface IStartContext
    {
        IProcessWrap Create(string command);
        
        IProcessResult Start(string command, string? standardInput = null);

        Task<IProcessResult> StartAsync(string command, string? standardInput = null);
    }
}