using System;
using System.Threading.Tasks;

namespace ShellToolkit
{
    public class ContainerContext : IStartContext
    {
        /* Properties */

        private readonly string _containerId;
        private readonly string _shell;

        /* Initialization */

        public ContainerContext(string containerId, string shell = ShellProcess.DefaultShell)
        {
            _containerId = containerId ?? throw new ArgumentNullException(nameof(containerId));
            _shell = shell;
        }

        /* Context */

        public IProcessWrap Create(string command) => new ContainerProcess(_containerId, _shell) {Command = command};

        public IProcessResult Start(string command, string? standardInput = null) =>
            Create(command).Start(standardInput);

        public Task<IProcessResult> StartAsync(string command, string? standardInput = null) =>
            Task.Run(() => Start(command, standardInput));
    }
}