using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ShellToolkit
{
    /// <summary>
    /// Executor for Son of Grid Engine running in Docker container<br/>
    /// Designed for particular Docker image
    /// <a href="https://hub.docker.com/r/robsyme/docker-sge">robsyme/docker-sge</a>
    /// </summary>
    public class ContainerProcess : IProcessWrap
    {
        /* Properties */

        private readonly string _containerId;
        private readonly ShellProcess _shellProcess;

        public Process Process => _shellProcess.Process;

        private string _command;

        public string Command
        {
            get => _command;
            set
            {
                _command = value;
                _shellProcess.Command =
                    $"docker exec --interactive \"{_containerId.Escape()}\" " +
                    $"su --login sgeuser --command \"{value.Escape()}\"";
            }
        }

        /* Initialization */

        public ContainerProcess(string containerId, string shell = ShellProcess.DefaultShell)
        {
            _containerId = containerId ?? throw new ArgumentNullException(nameof(containerId));
            _shellProcess = new ShellProcess {Shell = shell};
            Command = _command = string.Empty;
        }

        /* Process */

        public IProcessResult Start(string? standardInput = null) => _shellProcess.Start(standardInput);
        
        public Task<IProcessResult> StartAsync(string? standardInput = null) =>
            Task.Run(() => Start(standardInput));
    }
}