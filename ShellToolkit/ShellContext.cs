using System.Threading.Tasks;

namespace ShellToolkit
{
    public class ShellContext : IStartContext
    {
        /* Properties */
        
        public string? WorkingDirectory { get; set; }
        
        /* Context */
        
        public IProcessWrap Create(string command)
        {
            var process = new ShellProcess {Command = command};

            if (WorkingDirectory != null)
            {
                process.Process.StartInfo.WorkingDirectory = WorkingDirectory;
            }

            return process;
        }

        public IProcessResult Start(string command, string? standardInput = null) =>
            Create(command).Start(standardInput);

        public Task<IProcessResult> StartAsync(string command, string? standardInput = null) =>
            Task.Run(() => Start(command, standardInput));
    }
}