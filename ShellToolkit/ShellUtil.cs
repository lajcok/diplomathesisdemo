namespace ShellToolkit
{
    /// <summary>
    /// Static utility wrapper for <seealso cref="ShellProcess"/> functionality 
    /// </summary>
    public static class ShellUtil
    {
        /// <summary>
        /// Escapes string to be used in Shell in double quotes to be properly read<br/>
        /// Shorthand to <see cref="ShellProcess.Escape"/>
        /// </summary>
        /// <param name="content">String content to escape</param>
        /// <returns>Returns escaped content (without wrapping quotes)</returns>
        public static string Escape(this string content) => ShellProcess.Escape(content);

        /// <summary>
        /// Executes <c>Command</c> in <c>Shell</c> using <seealso cref="ShellProcess"/><br/>
        /// Shorthand to <see cref="ShellProcess.Start"/>
        /// </summary>
        /// <param name="command">Command to be executed</param>
        /// <param name="standardInput">Standard input to pass to <see cref="ShellProcess"/></param>
        /// <param name="shell">Shell to use for execution</param>
        /// <returns>Returns</returns>
        public static IProcessResult Execute(
            this string command,
            string? standardInput = default,
            string shell = ShellProcess.DefaultShell
        ) =>
            new ShellProcess
            {
                Command = command,
                Shell = shell,
            }.Start(standardInput);
    }
}