using System.Diagnostics;
using System.Threading.Tasks;

namespace ShellToolkit
{
    /// <summary>
    /// Wrapper of standard <see cref="Process"/> object
    /// </summary>
    public interface IProcessWrap
    {
        /// <summary>
        /// Reference to the wrapped <see cref="Process"/> object
        /// </summary>
        public Process Process { get; }
        
        /// <summary>
        /// Command being started
        /// </summary>
        public string Command { get; set; }

        /// <summary>
        /// Starts the command as a <see cref="Process"/>
        /// </summary>
        /// <param name="standardInput"><c>STDIN</c> passed to process</param>
        /// <returns>Returns result after finished execution</returns>
        public IProcessResult Start(string? standardInput = null);
        
        /// <summary>
        /// Starts the command as a <see cref="Process"/>
        /// </summary>
        /// <param name="standardInput"><c>STDIN</c> passed to process</param>
        /// <returns>Returns result after finished execution</returns>
        public Task<IProcessResult> StartAsync(string? standardInput = null);
    }
}