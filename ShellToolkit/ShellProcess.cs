using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace ShellToolkit
{
    /// <summary>
    /// Custom <see cref="Process"/> wrapper for Shell command execution
    /// </summary>
    public sealed class ShellProcess : IProcessWrap
    {
        /* Static Utils */

        /// <summary>
        /// Escapes string to be used in Shell in double quotes to be properly read
        /// </summary>
        /// <param name="content">String content to escape</param>
        /// <returns>Returns escaped content (without wrapping quotes)</returns>
        public static string Escape(string content) =>
            content
                .Replace("\\", "\\\\")
                .Replace("$", "\\$")
                .Replace("\"", "\\\"");

        /* Defaults */

        /// <summary>
        /// Default Shell to use for execution
        /// </summary>
        /// <value>
        /// Defaults to <c>/bin/bash</c>
        /// </value>
        public const string DefaultShell = "bash";

        /* Properties */

        public Process Process { get; } = new Process
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = "/usr/bin/env",
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            },
        };

        private StringBuilder output;
        private StringBuilder error;

        public string Shell { get; set; } = DefaultShell;

        private string _command = string.Empty;

        public string Command
        {
            get => _command;
            set
            {
                _command = value;
                Process.StartInfo.Arguments = $"{Shell} -c \"{Escape(Command)}\"";
            }
        }

        /* Methods */

        private void StartProcess(string? standardInput = null)
        {
            output = new StringBuilder();
            error = new StringBuilder();
            Process.OutputDataReceived += (_, e) => output.Append(e.Data + Environment.NewLine);
            Process.ErrorDataReceived += (_, e) => error.Append(e.Data + Environment.NewLine);

            Debug.WriteLine($"Executing: {Process.StartInfo.FileName} {Process.StartInfo.Arguments}");
            Process.Start();

            Process.BeginOutputReadLine();
            Process.BeginErrorReadLine();

            if (standardInput != null)
            {
                Process.StandardInput.Write(standardInput);
                Process.StandardInput.Flush();
                Process.StandardInput.Close();
            }
        }

        private IProcessResult CollectResult()
        {
            Process.WaitForExit();
            return new ProcessResult(Process, output.ToString(), error.ToString());
        }

        public IProcessResult Start(string? standardInput = null)
        {
            StartProcess(standardInput);
            return CollectResult();
        }

        public Task<IProcessResult> StartAsync(string? standardInput = null)
        {
            StartProcess(standardInput);
            return Task.Run(CollectResult);
        }
    }
}