﻿using System;
using System.IO;
using System.Linq;
using GridEngineAdapter;
using ShellToolkit;

namespace GridEngineDemo
{
    internal static class Program
    {
        private static void Main()
        {
            const string containerId = "diplomathesis_sge_1";
            Console.WriteLine($"Connecting Grid Engine in container {containerId}");
            Console.WriteLine();

            var container = new ContainerContext(containerId);
            var gridEngine = new JobManager(container);

            var jobs = gridEngine.List().ToArray();
            Console.WriteLine("Jobs queue:");
            Console.WriteLine(string.Join(
                string.Empty, jobs.Select(job => $"Job:{job.Uid} ({job.Name})"
                )));

            var assignment = new Job
            {
                Name = "testJob",
                Script = File.ReadAllText("../../../tail.sh"),
            };
            var subJob = gridEngine.Add(assignment);
            Console.WriteLine($"Submitted new {subJob.Uid}");
            Console.WriteLine();

            var delRes = gridEngine.Drop(subJob.Uid!);
            Console.WriteLine($"Deletion of {subJob.Uid} status: {delRes}");
        }
    }
}