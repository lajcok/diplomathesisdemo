const http = require('http')

const hostname = 'localhost'
const port = 3333

const dataLog = []

const server = http.createServer((req, res) => {
    let data = ''
    req.on('data', chunk => data += chunk)
    req.on('end', () => {
        const json = JSON.parse(data)
        console.log(json)
        dataLog.push(json)
    })

    res.statusCode = 200
    res.setHeader('Content-Type', 'text/plain')
    res.end('OK')
})

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`)
})

process.on('exit', () => {
    const fs = require('fs')
    const path = require('path')

    if (dataLog.length) {
        console.log('Saving weblog')
        const weblogDir = path.join(__dirname, 'weblog')
        if (!fs.existsSync(weblogDir)) {
            fs.mkdirSync(weblogDir)
        }
        fs.writeFileSync(path.join(weblogDir, `${Date.now()}.json`), JSON.stringify(dataLog))
        console.log('Saved')
    }
    else {
        console.log('Skipping saving, weblog empty')
    }
})

process.on('SIGINT', () => process.exit(0))
