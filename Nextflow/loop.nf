#!/usr/bin/env nextflow

params.n = 1000

process loop {
    """
    for i in {1..$params.n}
    do
        echo \$i
    done
    """
}
