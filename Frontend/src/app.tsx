import React from 'react'
import Axios from 'axios'
import {CContainer} from '@coreui/react'
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom'

import '@coreui/coreui/dist/css/coreui.min.css'
import {JobList} from './job/job.list'
import {FileUpload} from './r/file-upload'
import {RScript} from './r/r-script'
import {JobForm} from './job/job.form'


const axios = Axios.create({baseURL: '/api/'})

export const App: React.FunctionComponent = () => {
    const [upToDate, setUpToDate] = React.useState(true)
    React.useEffect(() => setUpToDate(true), [upToDate])
    const refresh = () => setUpToDate(false)
    
    return <CContainer id="App">
        <Router>
            <Switch>
                <Route path="/demo/r">
                    <section id="demo-r">
                        <h1>R Demo</h1>
                        <FileUpload {...{axios}}/>
                        <RScript {...{axios}}/>
                    </section>
                </Route>
                <Route path="/demo/jobs">
                    <section id="demo-jobs">
                        <h1>Jobs</h1>
                        <JobForm {...{axios}} onSubmit={refresh}/>
                        {upToDate ? <JobList {...{axios}}/> : undefined}
                    </section>
                </Route>
                <Route path="/">
                    <Redirect to={{pathname: '/demo/jobs'}}/>
                </Route>
            </Switch>
        </Router>
    </CContainer>
}
    