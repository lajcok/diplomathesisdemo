import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCardSubtitle,
    CCardTitle,
    CContainer,
    CForm,
    CFormControl,
    CFormLabel,
    CFormSelect,
    CSpinner,
} from '@coreui/react'
import {JsonForms} from '@jsonforms/react'
import {vanillaCells} from '@jsonforms/vanilla-renderers'
import {AxiosInstance} from 'axios'
import React from 'react'
import {coreUiRenderers} from '../forms'
import {IJob} from './job.model'

export const JobForm: React.FunctionComponent<{
    axios: AxiosInstance
    onSubmit?: (job: Partial<IJob>) => void
}> = ({axios, onSubmit}) => {
    const [jobTemplates, setJobTemplates] = React.useState<ReadonlyMap<string, IJobTemplate>>()
    const [jobData, setJobData] = React.useState<Partial<IJob>>({})
    const [validated, setValidated] = React.useState(false)
    const [submitting, setSubmitting] = React.useState(false)

    React.useEffect(() => {
        axios.get<readonly IJobTemplate[]>('JobTemplates').then(({data}) => {
            const templateMap = new Map(data.map(template => [template.name, template]))
            console.debug({templateMap})
            setJobTemplates(templateMap)
        })
    }, [])

    const formChange = (jobData: Partial<IJob>) => {
        console.debug({jobData})
        setJobData(prevState => ({...prevState, ...jobData}))
    }

    const templateChange = (title: string) => {
        const template = jobTemplates!.get(title)
        formChange({template})
    }
    
    const submitJob = () => {
        const {template, name, parameters} = jobData
        setSubmitting(true)
        axios.post(`Jobs/${template!.name}`, {name, parameters})
            .then(({data}) => {
                console.debug({submitSuccess: data})
                onSubmit?.(jobData)
                setJobData({})
                setValidated(false)
            })
            .catch(({data}) => console.debug({submitError: data}))
            .finally(() => setSubmitting(false))
    }

    return <CCard id="JobForm">
        <CCardHeader><h2>New job</h2></CCardHeader>
        <CCardBody>{
            jobTemplates
                ? <CForm
                    validated={validated}
                    onSubmit={event => {
                        event.preventDefault()
                        console.debug({jobData})

                        if (event.currentTarget.checkValidity()) {
                            submitJob()
                        }
                        setValidated(true)
                    }}
                    {...{noValidate: true}}
                >
                    <CCardTitle>General</CCardTitle>
                    <CContainer fluid className="mb-3">
                        <CFormLabel {...{htmlFor: 'name'}}>Name</CFormLabel>
                        <CFormControl
                            id="name"
                            onChange={event => formChange({name: event.currentTarget.value})}
                            disabled={submitting}
                        />
                    </CContainer>
                    <CCardTitle>Template</CCardTitle>
                    <CContainer fluid className="mb-3">
                        <CFormLabel {...{htmlFor: 'template'}}>Template</CFormLabel>
                        <CFormSelect
                            id="template"
                            onChange={event => templateChange(event.currentTarget.value)}
                            {...{value: jobData.template?.name ?? '', required: true, disabled: submitting}}
                        >
                            <option key={NaN} value="" hidden>select one</option>
                            {Array.from(jobTemplates.keys()).map((name, i) =>
                                <option key={i}>{name}</option>
                            )}
                        </CFormSelect>
                        {jobData.template && <JobProperties
                            template={jobData.template}
                            parametersData={jobData.parameters}
                            onChange={parameters => formChange({parameters})}
                            readonly={submitting}
                        />}
                    </CContainer>
                    <CButton type="submit" color="primary" disabled={submitting}>
                        Add {submitting ? <CSpinner size="sm"/> : undefined}
                    </CButton>
                </CForm>
                : <CSpinner size="sm"/>
        }</CCardBody>
    </CCard>
}

const JobProperties: React.FunctionComponent<{
    template: IJobTemplate,
    parametersData?: unknown,
    onChange?: (parameters: unknown) => void,
    readonly?: boolean,
}> = ({template, parametersData, onChange, readonly}) =>
    <div className="mt-3 mb-3">
        <CCardSubtitle>Properties</CCardSubtitle>
        <CContainer fluid className="mt-2">{
            template.parameters
                ? <JsonForms
                    schema={template.parameters}
                    renderers={coreUiRenderers}
                    cells={vanillaCells}
                    data={parametersData}
                    onChange={({data, errors}) =>
                        !(errors?.length ?? 0) ? onChange?.(data) : console.error({errors})
                    }
                    readonly={readonly}
                />
                : <p>Not parametrizable</p>
        }</CContainer>
    </div>
