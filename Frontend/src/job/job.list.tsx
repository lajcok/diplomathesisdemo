import React, {useEffect, useState} from 'react'
import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CTable,
    CSpinner,
} from '@coreui/react'
import {AxiosInstance} from 'axios'
import {IJob} from './job.model'

interface IProps {
    axios: AxiosInstance
}

export const JobList: React.FunctionComponent<IProps> = ({axios}) => {
    const [jobs, setJobs] = useState<IJob[]>()
    useEffect(() => {
        axios.get<IJob[]>('Jobs')
            .then(({data}) => setJobs(data))
    }, [])

    const [deleting, setDeleting] = useState<Set<string>>(new Set())
    const [deleted, setDeleted] = useState<Set<string>>(new Set())

    const onDelete = (uid: string) => () => {
        setDeleting(new Set(deleting).add(uid))
        axios.delete<{ success: boolean }>(`Jobs/${uid}`)
            .then(({data}) => {
                if (data.success) {
                    setDeleted(new Set(deleted).add(uid))
                } else {
                    console.error(`job ${uid} could not be deleted`)
                }
            })
            .catch(() => console.error(`job ${uid} could not be deleted`))
            .finally(() => {
                const delDeleting = new Set(deleting)
                if (delDeleting.delete(uid)) {
                    setDeleting(delDeleting)
                }
            })
    }

    return <CCard id="JobList">
        <CCardHeader>
            <h2>Queue</h2>
        </CCardHeader>
        <CCardBody>
            <CTable striped>
                <thead>
                <tr>
                    <th scope="col" style={{width: '20%'}}>UID</th>
                    <th scope="col">Name</th>
                    <th scope="col" style={{width: '10%'}}>Actions</th>
                </tr>
                </thead>
                {jobs
                    ? (jobs.length
                            ? <tbody>{
                                jobs.map(({uid, name}) =>
                                    <tr key={uid}>
                                        <th scope="row">{uid}</th>
                                        <td>{name}</td>
                                        <td>{
                                            !deleted.has(uid)
                                                ? (!deleting.has(uid)
                                                    ? <CButton
                                                        onClick={onDelete(uid)}
                                                        color="danger"
                                                        title="Delete job">&times;</CButton>
                                                    : <CSpinner size="sm"/>
                                                ) : <em>deleted</em>
                                        }</td>
                                    </tr>
                                )
                            }</tbody> : <caption>No jobs</caption>
                    ) : <caption><CSpinner size="sm"/></caption>
                }
            </CTable>
        </CCardBody>
    </CCard>
}
