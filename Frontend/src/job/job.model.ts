export interface IJob {
    uid: string
    name: string
    script?: string
    parameters?: unknown
    template?: IJobTemplate
}