import React from 'react'
import {CCardBody, CCardTitle} from '@coreui/react'

export const Plot: React.FunctionComponent = () =>
    <div>
        <CCardTitle>Plot Preview</CCardTitle>
        <CCardBody>
            <img src={`/api/file/plot.jpg?t=${new Date().getTime()}`}
                 alt="R Plot Preview" width={480} height={480}/>
        </CCardBody>
    </div>