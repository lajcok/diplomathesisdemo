import React from 'react'
import {AxiosInstance} from 'axios'
import {
    CButton,
    CButtonGroup,
    CCard,
    CCardBody,
    CCardHeader,
    CInputGroup,
    CFormControl,
    CSpinner,
} from '@coreui/react'
import {Plot} from './plot'

interface IProps {
    axios: AxiosInstance
}

interface IState {
    script: string
    running: boolean
    result?: string
}

export class RScript extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props)
        this.state = {
            script: 'c(1,2,3)',
            running: false,
        }
    }

    private readonly update = (event: React.FormEvent<HTMLTextAreaElement>) => {
        const script = event.currentTarget.value
        this.setState({script})
    }

    private readonly run = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        this.setState({running: true})
        this.props.axios.post('r', this.state.script, {
            headers: {'Content-Type': 'text/plain'},
            responseType: 'blob',
        })
            .then(({request}) => request.response)
            .then(blob => this.setState(prev => {
                if (prev.result) {
                    URL.revokeObjectURL(prev.result)
                }
                return {
                    result: URL.createObjectURL(blob),
                    running: false,
                }
            }))
    }

    readonly render = () =>
        <CCard id="RScript">
            <CCardHeader>
                <h2>R Script</h2>
            </CCardHeader>
            <CCardBody>
                <form onSubmit={this.run}>
                    <CInputGroup>
                        <CFormControl
                            component="textarea"
                            placeholder="R Script Here..."
                            value={this.state.script}
                            onChangeCapture={this.update}
                        />
                    </CInputGroup>
                    <CButtonGroup>
                        <CButton color="primary"
                                 type="submit"
                                 disabled={this.state.running}
                        >run</CButton>
                        <CButton color="secondary"
                                 type="button"
                                 href={this.state.result}
                                 disabled={!this.state.result}
                        >download</CButton>
                    </CButtonGroup>
                </form>
            </CCardBody>
            <CCardBody>
                {this.state.running ? <CSpinner/> : <Plot/>}
            </CCardBody>
        </CCard>

}