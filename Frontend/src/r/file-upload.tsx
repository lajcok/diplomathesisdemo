import React from 'react'
import {AxiosInstance} from 'axios'
import {CButton, CCard, CCardBody, CCardHeader} from '@coreui/react'

interface IProps {
    axios: AxiosInstance
}

interface IState {
    file?: File
    error?: string
}

export class FileUpload extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props)
        this.state = {}
    }

    private readonly update = (event: React.FormEvent<HTMLInputElement>) => {
        console.debug('file input: ', event.currentTarget.files)
        const file = event.currentTarget.files?.[0]
        this.setState({file})
    }

    private readonly upload = () => {
        if (this.state.file) {
            this.props.axios.post('file', this.state.file)
                .then(response => console.debug('file uploaded', response))
                .catch(error => {
                    console.error('upload failed', error)
                    this.setState({error: error.toString()})
                })
        }
    }

    readonly render = () =>
        <CCard id="FileUpload">
            <CCardHeader>
                <h2>Data Upload</h2>
            </CCardHeader>
            <CCardBody>
                <div>
                    <input type="file" onChange={this.update}/>
                    <CButton color="primary" onClick={this.upload} disabled={!this.state.file}>
                        upload
                    </CButton>
                </div>
                {this.state.error
                    ? <div>upload failed: {this.state.error}</div>
                    : undefined
                }
            </CCardBody>
        </CCard>

}