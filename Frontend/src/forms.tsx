import {CFormControlProps} from '@coreui/react/dist/components/form/CFormControl'
import React from 'react'
import {CInputGroup, CFormControl, CInputGroupText} from '@coreui/react'
import {withJsonFormsControlProps} from '@jsonforms/react'
import {
    ControlProps, isAnyOfControl, isIntegerControl, isMultiLineControl, isNumberControl, isPlainLabel,
    isStringControl,
    JsonFormsRendererRegistryEntry,
    rankWith
} from '@jsonforms/core'
import {vanillaRenderers} from '@jsonforms/vanilla-renderers'

interface InputControlProps extends ControlProps {
    path: string;
    data: string;
}

const htmlId = (id: string) => `${id}-input`

const labeledGroup = (Component: React.FunctionComponent<ControlProps>): React.FunctionComponent<ControlProps> =>
    ({id, label, ...props}) =>
        <CInputGroup size="sm">
            <CInputGroupText component="label" {...{htmlFor: htmlId(id)}}>{
                isPlainLabel(label) ? label : label.default
            }</CInputGroupText>
            <Component {...{id, label, ...props}}/>
        </CInputGroup>


function InputControl<T = string>(
    props?: Partial<Pick<CFormControlProps, 'type' | 'component'> & { parseValue: (value: string) => T }>
) {
    const {parseValue, ...rest} = {
        type: 'text',
        parseValue: (x: string) => x, ...props ?? {},
    }
    return withJsonFormsControlProps(labeledGroup(
        ({data, handleChange, path, id, uischema, schema, enabled, visible, required}: InputControlProps) =>
            <CFormControl
                id={htmlId(id)}
                component={isMultiLineControl(uischema, schema) ? 'textarea' : 'input'}
                onChange={(event) => handleChange(path, parseValue(event.currentTarget.value))}
                value={data ?? ''}
                disabled={!enabled}
                hidden={!visible}
                {...{
                    ...rest,
                    required,
                    minLength: schema.minLength,
                    maxLength: schema.maxLength,
                    min: schema.minimum,
                    max: schema.maximum,
                    pattern: schema.pattern,
                }}
            />
    ))
}

// TODO Create an input control for at least bool, enum OR a fallback

export const coreUiRenderers: JsonFormsRendererRegistryEntry[] = [
    ...vanillaRenderers,
    {tester: rankWith(3, isStringControl), renderer: InputControl()},
    {
        tester: rankWith(3, isIntegerControl),
        renderer: InputControl<number>({type: 'number', parseValue: parseInt}),
    },
] 
