const path = require('path');
const {ProvidePlugin} = require('webpack');

module.exports = {
    mode: 'development',
    devtool: 'source-map',
    entry: './src/',
    resolve: {
        extensions: ['.ts', '.tsx', '.js'],
        fallback: {
            buffer: require.resolve('buffer/'),
            http: require.resolve('stream-http'),
            https: require.resolve('https-browserify'),
        },
    },
    plugins: [
        new ProvidePlugin({
            process: 'process/browser',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                use: 'ts-loader',
            },
            {
                test: /\.htm(l?)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]'
                },
            },
            {
                test: /\.css$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]'
                },
            },
        ]
    },
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist'),
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 3000,
        proxy: {
            '/api': {
                target: 'https://localhost:5001',
                pathRewrite: {'^/api': ''},
                secure: false,
            },
        },
        historyApiFallback: {
            index: '/',
        },
    },
};
