# Diploma Thesis

_Jiří Lajčok, LAJ0013_

Source codes for diploma thesis with demos.
Documents and other related resources can be found in [Resources document](https://docs.google.com/document/d/1RNOyKriTq7WvE5B4REHzRi7mZUpVpNdz4FqKFfS05ho).

## Abstractions

The demos of different _job management technologies_ are meant to be based on the same _abstraction_.
This allows swapping of platform-dependent implementation modules of each technology without breaking the dependencies.

Description of related sub-projects in the solution follows:

- `JobManagement` – root abstractions of job management containing base interfaces
- `<technology>Adapter` – projects that implement the interfaces of `JobManagement` utilizing specific `<technology>`
- `<technology>Demo` – console programs that demonstrate the functionality and usage of given `<technology>Adapter`

## Backend

ASP.NET Core application providing REST API to control arbitrary `JobManagement` abstraction implementation.

### Setup

To run properly, setting environment variables for R.NET is necessary:

- `R_HOME` – top-level directory of the R installation
- `LD_LIBRARY_PATH` – shared R libraries path

## Frontend

Client side application providing UI to control Backend API.

### Setup

First, NPM dependencies have to be installed:

```
$ npm install
```

If backend is already up, frontend application can be served via: 

```
$ npm run dev
```

By default, development server is started at http://localhost:3000 with live reloading on.


