﻿using System;
using System.Linq;
using k8s;
using k8s.Models;
using KubernetesAdapter;
using Microsoft.Rest;

namespace KubernetesDemo
{
    internal static class Program
    {
        private static void Main()
        {
            var client = new Kubernetes(KubernetesClientConfiguration.BuildConfigFromConfigFile());
            var jobManager = new JobManager(client, "job-scripts-config", "jobs");

            var tailJob = jobManager.Add(new Job
            {
                Name = "tail",
                Script = "tail -f /dev/null",
            });

            Console.WriteLine(
                tailJob != null
                    ? $"Created job {tailJob.Name} with assigned"
                    : "Tail job could not be created"
            );

            var jobList = jobManager.List().ToArray();
            Console.WriteLine("All jobs listing:");
            foreach (var job in jobList)
            {
                Console.WriteLine($"\t{job.Name}");
            }

            var jobToDrop = tailJob ?? jobList.First();
            if (jobToDrop != null)
            {
                Console.WriteLine(
                    jobManager.Drop(jobToDrop.Uid)
                        ? $"Job {jobToDrop.Uid} has been dropped"
                        : $"Job {jobToDrop.Uid} has failed to drop"
                );
            }
            else
            {
                Console.WriteLine("No job to drop");
            }

            return;
        }
    }
}