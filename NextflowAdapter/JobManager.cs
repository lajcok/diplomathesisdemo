using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using JobManagement;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using ShellToolkit;

namespace NextflowAdapter
{
    public class JobManager : IJobManager
    {
        /* Constants */

# if DEBUG
        private const int WaitForErrorTimeout = Timeout.Infinite;
# else
        private const int WaitForErrorTimeout = 1000;
# endif

        public static readonly JSchema Parameters = new JSchema
        {
            Type = JSchemaType.Object,
            PatternProperties =
            {
                new KeyValuePair<string, JSchema>(
                    "^[A-Za-z0-9_]+$",
                    new JSchema
                    {
                        AnyOf =
                        {
                            new JSchema {Type = JSchemaType.String},
                            new JSchema {Type = JSchemaType.Number},
                            new JSchema {Type = JSchemaType.Boolean},
                        },
                    }
                ),
            },
        };

        /* Properties */

        private readonly IStartContext _context;

        /* Initialization */

        public JobManager(IStartContext? context = null, string? workingDirectory = null)
        {
            _context = context ?? new ShellContext {WorkingDirectory = workingDirectory};
        }

        /* Job Manager */

        private IEnumerable<IJob> List(bool finishedOnly)
        {
            var result = _context.Start("nextflow log | cut -f3,4,6");
            return NextflowCli.ParseJobsLog(result.StandardOutput, finishedOnly);
        }

        public IEnumerable<IJob> List() => List(true);

        public IJob Add(IJob job)
        {
            var runSource = job.ScriptFile != null ? $"'scripts/{job.ScriptFile}.nf'" : "-";
            var cmdBuilder = new StringBuilder($"nextflow run {runSource} -ansi-log false -dump-hashes");

            if (job.Name != null)
                cmdBuilder.Append($" -name '{job.Name}'");

            var fromTemplate = job is IJobFromTemplate jft ? jft : null;
            cmdBuilder.Append(
                string.Join(
                    string.Empty,
                    ParseParameters(fromTemplate?.Parameters).Select(
                        parameter => $" --{parameter.Key} '{parameter.Value}'"
                    )
                )
            );

            var process = _context.Create(cmdBuilder.ToString());
            var result = process.StartAsync(
                job.Script != null
                    ? $@"
process demo {{
    '''
    {job.Script}
    '''
}}
"
                    : null
            );
            var uid = NextflowCli.ParseCatchUid(process);
            if (uid == null)
            {
                var message = result.Wait(WaitForErrorTimeout)
                    ? result.Result.StandardError.Trim()
                    : "The job could not be created";
                throw new JobManagementException(message);
            }

            var name = List(false)
                .Where(j => j.Uid == uid)
                .Select(j => j.Name)
                .FirstOrDefault() ?? job.Name;

            return fromTemplate != null
                ? (IJob) new JobFromTemplate(fromTemplate) {Uid = uid, Name = name}
                : new Job(job) {Uid = uid, Name = name};
        }

        public bool Drop(string uid) => throw new NotImplementedException();

        /* Helpers */

        private static IDictionary<string, string> ParseParameters(JToken? parameters)
        {
            var valid = parameters?.IsValid(Parameters) ?? true;

            if (!valid)
            {
                throw new JobManagementException("Nextflow adapter only accepts single-level object mappings");
            }

            return parameters?.ToObject<IDictionary<string, string>>() ?? new Dictionary<string, string>();
        }
    }
}