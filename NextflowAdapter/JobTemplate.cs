using JobManagement;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace NextflowAdapter
{
    public class JobTemplate : IJobTemplate
    {
        /* Properties */

        public string Name { get; }
        public JSchema? Parameters { get; set; }
        public string ScriptFile { get; }

        /* Initialization */

        public JobTemplate(string title, string scriptFile)
        {
            Name = title;
            ScriptFile = scriptFile;
        }

        /* Methods */

        public IJobFromTemplate Derive(string? name, JToken? parameters = null)
        {
            if (Parameters != null)
            {
                if (!(parameters?.IsValid(Parameters) ?? false))
                {
                    throw new JobManagementException("Given parameters are invalid");
                }
            }
            else
            {
                if (parameters != null)
                {
                    throw new JobManagementException("No parameters expected");
                }
            }
            
            return new JobFromTemplate(this) {Name = name, ScriptFile = ScriptFile, Parameters = parameters};
        }
    }
}