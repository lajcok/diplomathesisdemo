using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using JobManagement;
using ShellToolkit;

namespace NextflowAdapter
{
    public static class NextflowCli
    {
# if DEBUG
        private const int ParseStreamTimeout = Timeout.Infinite;
# else
        private const int ParseStreamTimeout = 2000;
# endif


        private const int ParseStreamLinesLimit = 100;

        private static readonly Regex NextflowInitPattern = new Regex(
            @"^\s*N E X T F L O W  ~  version .+$",
            RegexOptions.Compiled
        );

        private static readonly Regex UuidPattern = new Regex(
            @"^\s*[0-9a-f]{32}\s+\[java\.util\.UUID\]\s+(?<uuid>[0-9a-f-]{36})\s*$",
            RegexOptions.Compiled
        );

        private static readonly Regex NamePattern = new Regex(
            @"^\s*Launching `[^`]*` \[(?<name>[\w_-])\] - revision: [0-9a-f]+\s*$",
            RegexOptions.Compiled
        );

        private static readonly Regex LogPattern = new Regex(
            @"^\s*(?<name>[\w_-]+)\s+(?<status>[\w-]+)\s+(?<uuid>[0-9a-f-]{36})\s*$",
            RegexOptions.Compiled
        );

        public static IEnumerable<IJob> ParseJobsLog(string standardOutput, bool finishedOnly = true) =>
            standardOutput
                .Split(Environment.NewLine).Skip(1)
                .Select(line => LogPattern.Match(line))
                .Where(match => match.Success)
                .Where(match => !finishedOnly || match.Groups["status"].Value == "-") // not finished jobs only
                .Select(match => new Job
                {
                    Uid = match.Groups["uuid"].Value,
                    Name = match.Groups["name"].Value,
                });

        public static string? ParseCatchUid(IProcessWrap process)
        {
            using var uuidCollection = new BlockingCollection<string>(1);
            var lineNumber = 0;

            void Handler(object _, DataReceivedEventArgs e)
            {
                if (
                    ++lineNumber > ParseStreamLinesLimit ||
                    lineNumber == 1 && (e.Data == null || !NextflowInitPattern.IsMatch(e.Data))
                )
                {
                    // ReSharper disable AccessToDisposedClosure
                    uuidCollection.CompleteAdding();
                    return;
                }

                if (e.Data == null) return;

                var uuidMatch = UuidPattern.Match(e.Data);
                if (!uuidMatch.Success) return;

                // ReSharper disable AccessToDisposedClosure
                uuidCollection.Add(uuidMatch.Groups["uuid"].Value);
                uuidCollection.CompleteAdding();
            }

            process.Process.OutputDataReceived += Handler;
            uuidCollection.TryTake(out var uid, ParseStreamTimeout);
            process.Process.OutputDataReceived -= Handler;

            return uid;
        }
    }
}