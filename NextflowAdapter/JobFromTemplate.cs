﻿using JobManagement;
using Newtonsoft.Json.Linq;

namespace NextflowAdapter
{
    public class JobFromTemplate : IJobFromTemplate
    {
        /* Properties */

        public string? Uid { get; set; }
        public string? Name { get; set; }
        public string? Script => null;
        public string? ScriptFile { get; set; }
        public JToken? Parameters { get; set; }
        public IJobTemplate Template { get; }

        /* Initialization */

        public JobFromTemplate(IJobTemplate template)
        {
            Template = template;
        }

        public JobFromTemplate(IJob job, IJobTemplate template) : this(template)
        {
            Uid = job.Uid;
            Name = job.Name;
            ScriptFile = job.ScriptFile;
        }

        public JobFromTemplate(IJobFromTemplate job) : this(job, job.Template)
        {
            Parameters = job.Parameters;
        }
    }
}