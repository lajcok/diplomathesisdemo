﻿using JobManagement;

namespace NextflowAdapter
{
    public class Job : IJob
    {
        public string? Uid { get; set; }
        public string? Name { get; set; }
        public string? Script { get; set; }
        public string? ScriptFile { get; set; }

        public Job()
        {
        }

        public Job(IJob job)
        {
            Uid = job.Uid;
            Name = job.Name;
            Script = job.Script;
        }
    }
}